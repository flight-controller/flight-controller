#include <cstdint>
#include <led.h>
#include <pico/stdlib.h>

led::led(void)
{
	gpio_init(PICO_DEFAULT_LED_PIN);
	gpio_set_dir(PICO_DEFAULT_LED_PIN, GPIO_OUT);
}

led::~led(void)
{
	gpio_deinit(PICO_DEFAULT_LED_PIN);
}

void led::blink(uint16_t delay)
{
    gpio_put(PICO_DEFAULT_LED_PIN, 0);
    sleep_ms(delay);
    gpio_put(PICO_DEFAULT_LED_PIN, 1);
    sleep_ms(delay);
}
